from attendance_app.src.duration import Duration


class Participant:

    def __init__(self, full_name: str, full_name_separete, join_time, leave_time, email: str, role: str, participant_id,
                 duration: Duration):
        self.full_name = full_name
        self.name = full_name_separete[0]
        self.middle_name = full_name_separete[1]
        self.first_surname = full_name_separete[2]
        self.second_surname = full_name_separete[3]
        self.join_time = join_time
        self.leave_time = leave_time
        self.email = email
        self.role = role
        self.id = participant_id
        self.in_meeting_duration = duration

    def __str__(self):
        new_participant = {'Full Name': self.full_name,
                           'Join time': self.join_time,
                           'Leave time': self.leave_time,
                           'In-meeting Duration': self.in_meeting_duration.dict_duration(),
                           'Email': self.email,
                           'Role': self.role,
                           'Participant ID (UPN)': self.id
                           }
        return new_participant

    def form_dict(self):
        new_participant = {'Full Name': self.full_name,
                           'Join time': self.join_time,
                           'Leave time': self.leave_time,
                           'In-meeting Duration': self.in_meeting_duration.dict_duration(),
                           'Email': self.email,
                           'Role': self.role,
                           'Participant ID (UPN)': self.id
                           }
        return new_participant
