class Summary:
    def __init__(self,title, id, participants, start_datetime, end_datetime, duration):
        self.title = title
        self.id = id
        self.attended_participants = participants
        self.start_datetime = start_datetime
        self.end_datetime = end_datetime
        self.in_meeting_duration = duration

    def form_dict(self):
        new_summary = {
            'Title': self.title,
            'Id': self.id,
            'Attended participants': self.attended_participants,
            'Start datetime': self.start_datetime,
            'End datetime': self.end_datetime,
            'Duration': self.in_meeting_duration.dict_duration()
        }
        return new_summary
