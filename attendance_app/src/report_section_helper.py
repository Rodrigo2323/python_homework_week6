def extract_section_rows(data, start_str, end_str):
    if len(end_str) == 0:
        end_str = '0'
        data.append([end_str])
    new_section_rows = [index for index, row in enumerate(
        data) if start_str in row or end_str in row]
    if len(new_section_rows) == 0:
        return
    return [(index, row) for index, row in enumerate(data) if row != [] and index > new_section_rows[0] and index < new_section_rows[1]]
