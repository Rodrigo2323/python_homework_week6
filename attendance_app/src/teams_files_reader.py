## TODO: Add whatever you need new classes or functions


# discovery de "data" files
def discover_files(folder='../data', find_pattern='.csv'):
    pass

def get_file_data(path):
    pass

def normalize_raw_data(data):
    pass

def load_data():
    data = []
    file_paths = discover_files() or []
    for fp in file_paths:
        raw_data = get_file_data(fp) # each element is a string (a line in the file)    
        data_dict = normalize_raw_data(raw_data)
        data.append(data_dict)
    return data
    # pass