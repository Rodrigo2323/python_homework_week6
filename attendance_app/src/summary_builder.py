from datetime import datetime

from attendance_app.src.duration import Duration
from attendance_app.src.summary import Summary


def normalize_summary(raw_data: dict):
    if 'Meeting Start Time' in raw_data:
        extra_case = especial_case(raw_data)
        return extra_case
    start = find_values('Start time', raw_data)
    end = find_values('End time', raw_data)
    duration = dif_time(start, end)
    summary_dict = {
        'Meeting Title': find_values('Meeting Title', raw_data),
        'Id': find_values('id', raw_data),
        'Attended participants': int(find_values('Attended participants', raw_data)),
        'Start Time': find_values('Start time', raw_data),
        'End Time': find_values('End time', raw_data),
        'Duration': {
            'hours': duration[0],
            'minutes': duration[1],
            'seconds': duration[2]
        }
    }
    return summary_dict


def especial_case(raw_data):
    start = find_values('Meeting Start Time', raw_data)
    end = find_values('Meeting End Time', raw_data)
    duration = dif_time(start, end)
    summary_dict = {
        'Meeting Title': find_values('Meeting Title', raw_data),
        'Id': raw_data.get('Meeting Id'),
        'Attended participants': int(find_values('Total Number of Participants', raw_data)),
        'Start datetime': raw_data.get('Meeting Start Time'),
        'End Time': raw_data.get('Meeting End Time'),
        'Duration': {
            'hours': int(duration[0]),
            'minutes': int(duration[1]),
            'seconds': int(duration[2])
        }
    }
    return summary_dict


def dif_time(start, end):
    list_duration = []
    start_time = datetime.strptime(start[0:-3].replace(",", ""), '%m/%d/%y %H:%M:%S')
    end_time = datetime.strptime(end[0:-3].replace(",", ""), '%m/%d/%y %H:%M:%S')
    duration = end_time - start_time
    list_duration.append(int(duration.seconds / 3600))
    list_duration.append((int(duration.seconds) / 60) % 60)
    list_duration.append(int(duration.seconds) % 60)
    if list_duration[0] < 1: list_duration[0] = 0
    if list_duration[1] < 1: list_duration[1] = 0
    return list_duration


def find_values(key, dict):
    if key in dict.keys():
        expected_value = dict[key]
        return expected_value
    return dict['Meeting Title']


def build_summary_object(raw_data: dict):
    new_dict = normalize_summary(raw_data)
    list_duration = dif_time(find_values('Start time', raw_data), find_values('End time', raw_data))
    duration = Duration(list_duration[0], int(list_duration[1]), int(list_duration[2]))
    new_summary = Summary(
        new_dict['Meeting Title'],
        raw_data.get('Id'),
        new_dict['Attended participants'],
        new_dict['Start Time'],
        new_dict['End Time'],
        duration
    )
    return new_summary
