from attendance_app.src.duration import Duration
from attendance_app.src.participant import Participant


def normalize_participant(data: dict):
    result = {'Name': data['Name'], 'First Join Time': data['First Join Time'],
              'Last Leave Time': data['Last Leave Time'],
              'In-meeting Duration': obtain_duration(data), 'Email': data['Email'], 'Role': data['Role'],
              'Participant ID (UPN)': data['Email']}
    return result


def obtain_duration(data):
    duration = []
    duration.append(data['In-meeting Duration'])
    time_1 = duration[0].split(' ')
    duration_time2 = hour_min_seg2(time_1)
    return duration_time2


def hour_min_seg2(data1):
    if len(data1) == 2:
        hour_1 = int(data1[0].translate(str.maketrans('', '', 'h')))
        min_1 = int(data1[1].translate(str.maketrans('', '', 'm')))
        seg_1 = 0
        return {'Hours': hour_1, 'Minutes': min_1, 'Seconds': seg_1}
    hour_1 = int(data1[0].translate(str.maketrans('', '', 'h')))
    min_1 = int(data1[1].translate(str.maketrans('', '', 'm')))
    seg_1 = int(data1[2].translate(str.maketrans('', '', 's')))
    return {'Hours': hour_1, 'Minutes': min_1, 'Seconds': seg_1}


def separate_full_name(full_name, email):
    email_name = email.split('@')[0].split('.')
    separate_name = full_name.split(' ')
    if email_name[-1] == separate_name[1]:
        separate_name.insert(1, None)
    if email_name[-1] == separate_name[-1]:
        separate_name.insert(3, None)
    return separate_name


def build_participant_object(raw: dict):
    if raw.get('Full Name') is None or raw.get('Join time') is None or raw.get('Leave time') is None:
        raise ValueError('Full Name, Join time, Leave time cannot be None')
    full_name_separate = separate_full_name(raw.get('Full Name'), raw.get('Email'))
    new_duration = Duration(raw['In-meeting Duration']['Hours'], raw['In-meeting Duration']['Minutes'],
                            raw['In-meeting Duration']['Seconds'])
    new_participant = Participant(raw['Full Name'], full_name_separate, raw['Join time'], raw['Leave time'],
                                  raw['Email'], raw['Role'], raw['Participant ID (UPN)'], new_duration)
    print(new_participant.form_dict())
    return new_participant


# def build_participant_object(raw: dict):
#     if raw.get('Full Name') is None or raw.get('Join time') is None or raw.get('Leave time') is None:
#         raise ValueError('Full Name, Join time, Leave time cannot be None')
#     return Participant(
#         raw['Full Name'],
#         raw['Join time'],
#         raw['Leave time'],
#         Duration(
#             raw['In-meeting Duration']['Hours'],
#             raw['In-meeting Duration']['Minutes'],
#             raw['In-meeting Duration']['Seconds']),
#         raw['Email'],
#         raw['Role'],
#         raw['Participant ID (UPN)'])


def resolve_participant_join_last_time(data: dict):
    result = {'Name': obtein_name(data), 'First Join Time': obtein_first_join(data),
              'Last Leave Time': obtain_last_join(
                  data), 'In-meeting Duration': obtain_sum_duration(data), 'Email': obtein_email(data),
              'Role': obtein_role(data), 'Participant ID (UPN)': obtein_id(data)}
    return result


def obtain_sum_duration(data):
    duration = []
    for dictionary in data:
        duration.append(dictionary['Duration'])
    time_1 = duration[0].split(' ')
    time_2 = duration[1].split(' ')
    duration_time = hour_min_seg(time_1, time_2)
    return duration_time


def hour_min_seg(data1, data2):
    min_1 = int(data1[0].translate(str.maketrans('', '', 'm')))
    min_2 = int(data2[0].translate(str.maketrans('', '', 'm')))
    seg_1 = int(data1[1].translate(str.maketrans('', '', 's')))
    seg_2 = int(data2[1].translate(str.maketrans('', '', 's')))
    string_time = ''
    sum_h = 0
    sum_m = min_1 + min_2
    sum_s = seg_1 + seg_2
    if sum_s > 60:
        sum_m = 1 + sum_m
        sum_s = sum_s - 60
    if sum_m > 60:
        sum_h = 1 + sum_h
        sum_m = sum_m - 60
    if sum_h < 0:
        return (str(sum_m) + 'm' + ' ' + str(sum_s) + 's')
    return (str(sum_h) + 'h' + ' ' + str(sum_m) + 'm' + ' ' + str(sum_s) + 's')


def obtein_name(data):
    name = []
    for dictionary in data:
        name.append(dictionary['Full Name'])
    return name[0]


def obtein_first_join(data):
    join = []
    for dictionary in data:
        join.append(dictionary['Join Time'])
    return join[0]


def obtain_last_join(data):
    leave = []
    for dictionary in data:
        leave.append(dictionary['Leave Time'])
    return leave[1]


def obtein_email(data):
    email = []
    for dictionary in data:
        email.append(dictionary['Email'])
    return email[0]


def obtein_role(data):
    role = []
    for dictionary in data:
        role.append(dictionary['Role'])
    return role[0]


def obtein_id(data):
    id_number = []
    for dictionary in data:
        id_number.append(dictionary['Participant ID (UPN)'])
    return id_number[0]
