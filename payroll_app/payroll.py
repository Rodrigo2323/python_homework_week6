class PayrollSystem:

    def calculate_payroll(self, employees):
        if employees is None or len(employees) == 0: return None
        for employee in employees:
            print(f'Payroll for: {employee}')
            print(f'Check amount: {employee.calculate_payroll()}')
