from employee import *

def test_admin_worker():
    create_new_admin_worker = AdmistrativeWorker(2323, 'New Admin Worker', 2000)
    assert type(create_new_admin_worker) is AdmistrativeWorker


def test_calc_payroll_admin_worker():
    new_admin_worker = AdmistrativeWorker(2323, 'New Admin Worker', 2000)
    expected_salary = 2000
    assert expected_salary == new_admin_worker.calculate_payroll()


def test_sales_associated():
    new_sales_associated = SalesAssociate(2323, 'New Sales Association', 1000, 250)
    assert type(new_sales_associated) is SalesAssociate


def test_calc_payroll_sales_associate():
    new_sales_associated = SalesAssociate(2323, 'New Sales Association', 1000, 250)
    expectedSalary = 1000 + 250
    assert expectedSalary == new_sales_associated.calculate_payroll()


def test_maufactaurin_worker():
    new_maufactaurin_worker = ManufacturingWorker(2323, 'New ManufacturingWorker', 15, 40)
    expected_salary = 15 * 40
    assert expected_salary == new_maufactaurin_worker.calculate_payroll()