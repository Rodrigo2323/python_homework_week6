from employee import *


class EmployeeDatabase:
    def __init__(self):
        self.__employees = [
            {
                'id': 1,
                'name': 'John Doe',
                'role': 'manager',  # is a Admistrative Worker
                'weekly_salary': 1500
            },
            {
                'id': 2,
                'name': 'Jane Doe',
                'role': 'factory',  # is a Manufacturing Worker
                'worked_hours': 40,
                'hour_rate': 15
            },
            {
                'id': 3,
                'name': 'Foo Fighter',
                'role': 'sales',  # is a Sales Associate
                'fixed_salary': 1000,
                'comission': 250
            }
        ]

    def get_employees(self):
        """Returns a list of objects whose parent is class Employee"""
        return [self.__create_employee(employee) for employee in self.__employees]

    def __create_employee(self, raw_data):

        if raw_data.get_role('role') == 'manager':
            return AdmistrativeWorker(raw_data.get('id'), raw_data.get('name'), raw_data.get('weekly_salary'))
        elif raw_data.get('role') == 'factory':
            return ManufacturingWorker(raw_data.get('id'), raw_data.get('name'), raw_data.get('worked_hours'),
                                       raw_data.get('hour_rate'))

        elif raw_data.get('role') == 'sales':
            return SalesAssociate(raw_data.get('id'), raw_data.get('name'), raw_data.get('fixed_salary'),
                                  raw_data.get('comission'))
